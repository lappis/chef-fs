name "firewall_server"
description "firewall machine"

run_list *[
  'recipe[basics]',
  'recipe[firewall]'
]
